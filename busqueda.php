<?php
require 'database.php';
$id = null;
if (!empty($_GET['id'])) {
    $id = $_REQUEST['id'];
}
if ($id == null) {
    header("Location: denuncias.php");
} else {
    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT idDenunciaConEstatus, nombreEstatus, fechaSucedio, narrativa FROM denunciasConEstatus NATURAL JOIN estatusDenuncia,4cuandoSucedio,2queSucedio WHERE idDenunciaConEstatus = ? AND idDenunciaConEstatus = idQueSucedio AND idDenunciaConEstatus = idCuandoSucedio AND idEstatusDeDenuncia=idestatusDenuncia;";
    $q = $pdo->prepare($sql);
    $q->execute(array($id));
    $data = $q->fetch(PDO::FETCH_ASSOC);
    Database::disconnect();
}
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
</head>

<body>
    </br>
    <!-- div que contiene la barra de navegación -->
    <section class="hero-head">
        <div id="nav-bar"></div>
    </section>

    <section class="hero-body">
        <div class="columns">
            <!-- div que contiene el icono de flecha para regresar a ventana anterior -->
            <div class="column">
                <div class="icon is-large">
                    <a href="index.php">
                        <img src="img/flecha.png"></img>
                    </a>
                </div>
            </div>

            <div class="column mr-6 mt-4">
                <p class="title is-2 has-text-centered">Búsqueda</p>
            </div>

            <div class="column is-3 mr-6 ml-5 mt-4">
                <!-- div que contiene el input y search para buscar un folio -->
                <div class="field has-addons">
                    <form action="busqueda.php" method="get">
                        <div class="control">
                            <input name="id" class="input" type="text" placeholder="Ingresa folio de denuncia">
                        </div>
                        <div class="control">
                            <button class="button buttonB">Buscar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Inicio de div que contiene modulo de busqueda -->
        <div class="formulario busqueda">
            <p class="subtitle is-5 has-text-justified pt-5 mx-6">
                En esta sección encontrarás información más detallada sobre una denuncia.
            </p>

            <!-- Inicio de fila de 2 columnas para la presentacion de field de codigo postal, clave electoral y botones -->
            <div class="columns is-centered mt-6">
                <div class="box" style="width: 700px; height: 460px;" name="busqueda">

                    <div class="columns mt-4 is-centered">

                        <label class="label mx-5">Folio No.</label>
                        <label class="checkbox">
                            <?php echo $data['idDenunciaConEstatus']; ?>
                        </label>

                        <label class="label mx-5">Fecha:</label>
                        <label class="checkbox">
                            <?php echo $data['fechaSucedio']; ?>
                        </label>

                        <label class="label  mx-5">Estatus:</label>
                        <label class="checkbox">
                            <?php echo $data['nombreEstatus']; ?>
                        </label>

                    </div>

                    <div class="column resumenSucedido">
                        <label class="label">Resumen de lo sucedido:</label>
                        <label class="checkbox">
                            <?php echo $data['narrativa']; ?>
                        </label>
                    </div>

                </div>

            </div>


        </div>
        <!-- Fin de div que contiene modulo de busqueda -->
    </section>
    </br>
    </br>
    </br>
    </br>

    <section class="hero-foot pt-6">
        <!-- div que contiene el footer -->
        <div id="footer"></div>
    </section>


</body>

<!-- Script que carga los archivos -->
<script>
    $("#nav-bar").load("shared/navbar2.php");
    $("#footer").load("shared/footer.php");
</script>

</html>