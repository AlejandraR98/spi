<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
</head>

<body>
    <!-- div que contiene la barra de navegación -->
    <div id="nav-bar"></div>
    </br>
    </br>
    <!-- Inicio de div que contiene cards en columnas para la implementación de lecturas -->
    <div class="lecturas">
        <h1 class="title is-2 has-text-centered has-text-white has-text-weight-semibold pt-6">Lecturas Destacadas</h1>
        </br>
        </br>
        <!-- Inicio de primera fila de 4 columnas para la presentación de lecturas -->
        <div class="columns px-6">

            <!-- Inicio de columna para card # 1 -->
            <div class="column">
                <a href="informatec.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">InformaTEC</p>
                                    <p class="subtitle is-6">Lo mejor de InformaTEC</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 1 -->

            <!-- Inicio de columna para card # 2 -->
            <div class="column">
                <a href="panorama.php">
                    <div class="card " style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content ">
                                    <p class="title is-4">Panorama general de la corrupción</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 2 -->

            <!-- Inicio de columna para card # 3 -->
            <div class="column">
                <a href="detallesLectura2.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Investigaciones</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 3 -->

            <!-- Inicio de columna para card # 4 -->
            <div class="column">
                <a href="catalogodelitos.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Catálogo de delitos electorales</p>
                                    <p class="subtitle is-6">?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 4 -->
        </div>
        <!--  Fin de primera fila de 4 columnas para la presentación de lecturas -->

        <!-- Inicio de segunda fila de 4 columnas para la presentación de lecturas -->
        <div class="columns px-6">

            <!-- Inicio de columna para card # 5 -->
            <div class="column">
                <a href="catalogocorrupcion.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Catálogo de delitos por hechos de corrupción.</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 5 -->

            <!-- Inicio de columna para card # 6 -->
            <div class="column">
                <a href="detallesLectura6.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Catálogo de faltas graves/no graves cometidas por servidores públicos.
                                    </p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 6 -->

            <!-- Inicio de columna para card # 7 -->
            <div class="column">
                <a href="detallesLectura7.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Agenda Civil de Combate a la Corrupción</p>
                                    <p class="subtitle is-6">Infografías</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 7 -->

            <!-- Inicio de columna para card # 8 -->
            <div class="column">
                <a href="detallesLectura8.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">E-book “Formando Incorruptibles”.</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 8 -->
        </div>
        <!--  Fin de segunda fila de 4 columnas para la presentación de lecturas -->

        <!-- Inicio de primera fila de 4 columnas para la presentación de lecturas -->
        <div class="columns px-6">

            <!-- Inicio de columna para card # 1 -->
            <div class="column">
                <a href="informatec.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Noticiero Incorruptible.
                                    </p>
                                    <p class="subtitle is-6">Lo mejor de InformaTEC</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 1 -->

            <!-- Inicio de columna para card # 2 -->
            <div class="column">
                <a href="detallesLectura2.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Eventos realizados.</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 2 -->

            <!-- Inicio de columna para card # 3 -->
            <div class="column">
                <a href="detallesLectura3.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Experiencias en servicio social.</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 3 -->

            <!-- Inicio de columna para card # 4 -->
            <div class="column ">
                <a href="detallesLectura4.php">
                    <div class="card" style="height:100%">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4">Lectura # 4</p>
                                    <p class="subtitle is-6">Autor de lectura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Fin de columna para card # 4 -->
        </div>
        <!--  Fin de primera fila de 4 columnas para la presentación de lecturas -->
        </br>
        </br>
        </br>
        </br>
    </div>
    <!-- Fin de div que contiene cards en columnas para la implementación de lecturas -->
</body>

<!-- Script que carga los archivos -->
<script>
    $("#nav-bar").load("shared/navbar.php");
</script>

</html>