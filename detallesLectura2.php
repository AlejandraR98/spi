<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
</head>

<body>
    </br>
    <!-- div que contiene la barra de navegación -->
    <section class="hero-head">
        <div id="nav-bar"></div>
    </section>

    <section class="hero-body">
        <div class="icon is-large">
            <a href="index.php">
                <img src="img/flecha.png"></img>
            </a>
        </div>
        <!-- Inicio de div que contiene el contenido de lectura #1 -->
        <div class="detalle1">
            </br>
            </br>

            <!-- Inicio de fila de 2 columnas para la presentación de lecturas -->
            <div class="columns">
                <div class="column mx-6">
                    <h1 class="title is-2 has-text-centered has-text-weight-semibold pt-2">Lectura2</h1>
                    <p class="title is-5 is-italic pb-5">Autor de lectura</p>
                    <p class="subtitle is-6 has-text-justified">
                        Et malesuada fames ac turpis. Sed nisi lacus sed viverra tellus in hac. Pharetra et ultrices neque ornare
                        aenean euismod elementum. Etiam erat velit scelerisque in dictum non consectetur. Sem viverra aliquet eget
                        sit amet tellus cras adipiscing. Enim diam vulputate ut pharetra. Sem integer vitae justo eget magna fermentum iaculis.
                        Felis donec et odio pellentesque diam volutpat. Non curabitur gravida arcu ac tortor dignissim convallis aenean.
                        Pretium fusce id velit ut tortor. Adipiscing elit ut aliquam purus sit amet luctus venenatis lectus. Mauris a diam
                        maecenas sed enim ut sem viverra. Amet luctus venenatis lectus magna fringilla urna porttitor rhoncus dolor. Orci eu
                        lobortis elementum nibh. Tempus iaculis urna id volutpat lacus. Integer enim neque volutpat ac tincidunt vitae semper
                        quis lectus. Nunc aliquet bibendum enim facilisis gravida neque convallis a cras. Et molestie ac feugiat sed lectus vestibulum.
                    </p>
                </div>
                <div class="column mx-6">
                    <figure class="image is-5by4">
                        <img src="https://bulma.io/images/placeholders/128x128.png">
                    </figure>
                </div>
            </div>
            <!-- Fin de fila de 2 columnas para la presentación de lecturas -->

        </div>
        <!-- Fin de div que contiene el contenido de lectura #1 -->
    </section>

</body>

<!-- Script que carga los archivos -->
<script>
    $("#nav-bar").load("shared/navbar.php");
</script>

</html>