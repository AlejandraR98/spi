<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
</head>

<body>
    <!-- div que contiene la barra de navegación -->
    <div id="nav-bar"></div>
    <div id="descripcion" name="descripcion">
        </br>
        </br>
        </br>
        </br>
        <div class="content has-text-centered pl-3 pr-5">
            <h2>
                Elección sin corrupción
            </h2>
        </div>
        <div class="columns is-centered mt-4">
            <img class="img-logo mx-6" src="img/logo3.png" width="130" height="130">
        </div>
        <!-- Box que contiene la info de acerca de -->
        <div class="columns is-centered mt-4">
            <div class="box has-text-centered" style="width: 950px;">
                <p class="title is-4 has-text-weight-semibold has-text-centered ">¿Qué hacemos y qué no hacemos?</p>
                </br>
                <p class="subtitle is-size-4 has-text-justified">
                    El objetivo final de este proyecto es contribuir con las autoridades para evitar actos de corrupción en cuestión de delitos electorales. Somos una plataforma que funciona como medio para brindar información asociada al proceso electoral, así como recibir denuncias y llevarlas con las autoridades correspondientes. No somos ni tenemos ninguna clase de autoridad para investigar y castigar ninguna clase de conducta que aquí sea denunciada, las autoridades serán las encargadas de darle el seguimiento necesario. Siempre es mejor denunciar directamente en las fiscalías especializadas en delitos electorales (FEDENET), pero ofrecemos esta vía confiable, segura, ágil y sencilla para que realices una denuncia efectiva.
                </p>
            </div>
        </div>

        <div class="content has-text-centered">

            <a href="denuncias.php">
                <button id="btnDenuncia" class="button buttonB is-medium is-rounded" type="submit" name="submit" style="width: 200px; height: 70px;">
                    Denuncia aquí
                </button>
            </a>
        </div>
        </br>
        </br>
        </br>
        <!-- Fin del box que contiene la info de acerca de -->

    </div>

    <section class="hero-foot pt-6">
        <!-- div que contiene el footer -->
        <div id="footer"></div>
    </section>

</body>

<!-- Script que carga los archivos -->
<script>
    $("#nav-bar").load("shared/navbar2.php");
    $("#lecturas").load("lecturas.php");
    $("#footer").load("shared/footer.php");
</script>

</html>