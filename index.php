<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
</head>

<body>
    <!-- div que contiene la barra de navegación -->
    <div id="nav-bar"></div>


    <!-- div que contiene el carrusel de imagenes -->
    <div id="carouselIm"></div>

    <div id="descripcion-grupo" name="descripcion-grupo">

        <!-- Box que contiene la descripción del grupo estudiantil -->
        <div class="content has-text-centered pl-3 pr-5">
            <h2 class"title is-1">
                Solo Para Incorruptibles
            </h2>
            <h2>
                Asociación estudiantil multidisciplinaria del Tecnológico de Monterrey, dedicada a promover el análisis del fenómeno de la corrupción,
                y la generación de propuestas para combatirla; creando redes de colaboración entre EXATEC, academia, gobierno, medios de comunicación,
                asociaciones civiles, sector privado, organismos internacionales y Organizaciones No Gubernamentales.
            </h2>
        </div>
        <!-- Fin del box -->
        </br>
        </br>
        </br>
        </br>
        </br>
        <!-- Box que contiene misión y visión del grupo -->
        <div class="columns about pr-3">
            <div class="column">
                <div class="box about" style="width: 540px;">
                    <p class="title is-4 has-text-weight-semibold has-text-centered ">Misión</p>
                    <p class="subtitle is-6 has-text-justified">
                        Generar consciencia sobre el impacto económico, político y social de la corrupción, para desarrollar estrategias de combate, mediante una participación ciudadana activa, que detone un cambio social sostenible en México.
                    </p>
                </div>
            </div>
            <div class="column mr-6">
                <div class="box about" style="width: 540px;">
                    <p class="title is-4 has-text-weight-semibold has-text-centered">Visión</p>
                    <p class="subtitle is-6 has-text-justified">
                        Posicionar al grupo estudiantil como un modelo diferenciador incluyente, efectivo, abierto y participativo en la obtención de conocimiento colectivo en materia anticorrupción, involucrando positivamente a todas las carreras del Tecnológico de Monterrey, para lograr el compromiso social de impactar en la vida pública de México.
                    </p>
                </div>
            </div>
        </div>
        <!-- Fin del box que contiene misión y visión del grupo -->
        </br>
        </br>
        </br>
        </br>
        </br>
    </div>

    <!-- div que contiene lecturas -->
    <div id="lecturas"></div>

    <!-- div que contiene el footer -->
    <div id="footer"></div>

</body>

<!-- Script que carga los archivos -->
<script>
    $("#nav-bar").load("shared/navbar.php");
    $("#carouselIm").load("shared/carousel.php");
    $("#lecturas").load("lecturas.php");
    $("#footer").load("shared/footer.php");
</script>

</html>