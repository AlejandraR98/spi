<?php

require 'database.php';

$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'denuncias' AND TABLE_NAME = '1datosDenunciante'";
$q = $pdo->prepare($sql);
$q->execute();
$data = $q->fetch(PDO::FETCH_ASSOC);
$nextID = $data['AUTO_INCREMENT'];
Database::disconnect();

//Para búsqueda
$idSearch = null;

//Datos Iniciales
$codigoPostalError = null;
$seccionElectoralError = null;
$idSexoError   = null;
$idOcupacionError   = null;
$idEscolaridadError   = null;

//Primera Pregunta
$narrativaError = null;

//Segunda Pregunta
$estadoSucedioError = null;
$municipioSucedioError = null;

//Tercer Pregunta
$fechaSucedioError = null;
$horaAproxSucedioError = null;

//Cuarta Pregunta 
$nombreQuienComError = null;
$empresaQuienComError = null;
$puestoQuienComError = null;

//Quinta pregunta

if (!empty($_POST)) {

    // keep track post values 

    //Datos Iniciales
    $codigoPostal = $_POST['codigoPostal'];
    $seccionElectoral = $_POST['seccionElectoral'];
    $idOcupacion  = $_POST['idOcupacion'];
    $idEscolaridad  = $_POST['idEscolaridad'];
    $idSexo = $_POST['idSexo'];

    //Primera Pregunta
    $narrativa = $_POST['narrativa'];

    //Segunda Pregunta
    $estadoSucedio = $_POST['estadoSucedio'];
    $municipioSucedio = $_POST['municipioSucedio'];

    //Tercer Pregunta
    $fechaSucedio = isset($_POST['fechaSucedio']);
    $horaAproxSucedio = isset($_POST['horaAproxSucedio']);

    //Cuarta Pregunta
    $nombreQuienCom = $_POST['nombreQuienCom'];
    $empresaQuienCom = $_POST['empresaQuienCom'];
    $puestoQuienCom = $_POST['puestoQuienCom'];

    //Quinta Pregunta
    $compraVotos = $_POST['compraVotos'];
    $tomaCredencial = $_POST['tomaCredencial'];
    $condicionarServ = $_POST['condicionarServ'];
    $alterarResultados = $_POST['alterarResultados'];
    $roboCasillas = $_POST['roboCasillas'];
    $amenazaServPub = $_POST['amenazaServPub'];
    $otroConductaCom = $_POST['otroConductaCom'];

    // validate input
    $valid = true;

    //Datos Iniciales
    if (empty($codigoPostal)) {
        $codigoPostalError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($seccionElectoral)) {
        $seccionElectoralError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($idOcupacion)) {
        $idOcupacionError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($idEscolaridad)) {
        $idEscolaridadError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($idSexo)) {
        $idSexoError = 'Campo necesario!';
        $valid = false;
    }

    //Primera Pregunta
    if (empty($narrativa)) {
        $narrativaError = 'Campo necesario!';
        $valid = false;
    }

    //Segunda Pregunta
    if (empty($estadoSucedio)) {
        $estadoSucedioError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($municipioSucedio)) {
        $municipioSucedioError = 'Campo necesario!';
        $valid = false;
    }

    //Tercer Pregunta
    if (empty($fechaSucedio)) {
        $fechaSucedioError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($horaAproxSucedio)) {
        $horaAproxSucedioError = 'Campo necesario!';
        $valid = false;
    }

    //Cuarta Pregunta
    if (empty($nombreQuienCom)) {
        $nombreQuienComError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($empresaQuienCom)) {
        $empresaQuienComError = 'Campo necesario!';
        $valid = false;
    }
    if (empty($puestoQuienCom)) {
        $puestoQuienComError = 'Campo necesario!';
        $valid = false;
    }

    // insert data
    if ($valid) {
        var_dump($_POST);
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //Datos Iniciales
        $sql = "INSERT INTO 1datosDenunciante (idDatosDenunciante, codigoPostal, seccionElectoral, ocupacion, escolaridad, sexo) values(null, ?, ?, ?,?,?)";
        $q = $pdo->prepare($sql);
        $q->execute(array($codigoPostal, $seccionElectoral, $idOcupacion, $idEscolaridad, $idSexo));
        //printf("Last inserted record has id %d\n", mysql_insert_id());

        //Primera Pregunta
        $sqlP1 = "INSERT INTO 2queSucedio (idQueSucedio, narrativa) values(null, ?)";
        $qP1 = $pdo->prepare($sqlP1);
        $qP1->execute(array($narrativa));

        //Segunda Pregunta
        $sqlP2 = "INSERT INTO 3dondeSucedio (idDondeSucedio, estadoSucedio, municipioSucedio) values(null, ?, ?)";
        $qP2 = $pdo->prepare($sqlP2);
        $qP2->execute(array($estadoSucedio, $municipioSucedio));

        //Tercer Pregunta
        $sqlP3 = "INSERT INTO 4cuandoSucedio (idCuandoSucedio, fechaSucedio, horaAproxSucedio) values(null, ?, ?)";
        $qP3 = $pdo->prepare($sqlP3);
        $qP3->execute(array($fechaSucedio, $horaAproxSucedio));

        //Cuarta Pregunta
        $sqlP4 = "INSERT INTO 5quienCometio (idQuienCometio, nombreQuienCom, empresaQuienCom, puestoQuienCom) values(null, ?, ?, ?)";
        $qP4 = $pdo->prepare($sqlP4);
        $qP4->execute(array($nombreQuienCom, $empresaQuienCom, $puestoQuienCom));

        //Quinta Pregunta
        if (empty($otroConductaCom)) {
            $otroConductaCom = null;
        }
        $sqlP5 = "INSERT INTO 6queConductaCom (idConductaCometio, compraVotos, tomaCredencial, condicionarServ, alterarResultados, roboCasillas, amenazaServPub, otroConductaCom) values(null, ?, ?, ?, ?, ?, ?, ?)";
        $qP5 = $pdo->prepare($sqlP5);
        $qP5->execute(array($compraVotos, $tomaCredencial, $condicionarServ, $alterarResultados, $roboCasillas, $amenazaServPub, $otroConductaCom));

        //Insercion Tabla Denuncias con Estatus
        $sqlP6 = "INSERT INTO denunciasConEstatus (idDenunciaConEstatus, idEstatusDeDenuncia) values(null, 1)";
        $qP6 = $pdo->prepare($sqlP6);
        $qP6->execute(array());

        Database::disconnect();
        header("Location: index.php");
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
    <script src="bulma-calendar/dist/js/bulma-calendar.min.js"></script>
    <!-- cdn de librería de calendar de bulma -->
    <link href="bulma-calendar/dist/css/bulma-calendar.min.css" rel="stylesheet">
</head>

<!-- CSS que elimina el spin de numeros -->
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type="number"] {
        -moz-appearance: textfield;
    }
</style>

<body>
    </br>
    <!-- div que contiene la barra de navegación -->
    <section class="hero-head">
        <div id="nav-bar"></div>
    </section>

    <section class="hero-body">
        <div class="columns">
            <!-- div que contiene el icono de flecha para regresar a ventana anterior -->
            <div class="column">
                <div class="icon is-large">
                    <a href="index.php">
                        <img src="img/flecha.png"></img>
                    </a>
                </div>
            </div>
            <div class="column mr-6 mt-4">
                <p class="title is-2 has-text-centered">Denuncias</p>
            </div>

            <div class="column is-3 mr-6 ml-5 mt-4">
                <!-- div que contiene el input y search para buscar un folio -->
                <form action="busqueda.php" method="get">
                    <div class="field has-addons">
                        <div class="control">
                            <input name="id" class="input" type="text" placeholder="Ingresa folio de denuncia">
                        </div>
                        <div class="control">
                            <button class="button buttonB">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <p class="subtitle has-text-centered is-size-9 pt-3 mx-6">
            Esta es la sección de denuncias, favor de llenar el siguiente formulario con los datos correspondientes.
        </p>
        </br>
        <div>
            <div class="divider">Información Inicial</div>
        </div>

        <div class="columns is-centered mt-4">
            <form action="" method="POST" name="form">

                <div class="column is-offset-10">
                    <div class="icon">
                        <a id="iconHelp">
                            <img src="img/help.png"></img>
                        </a>
                    </div>
                </div>

                <div class="columns is-centered mx-6">
                    <div class="column is-one-fifth mx-4">
                        <div>
                            <label class="label is-size-5">No. Denuncia</label>
                        </div>
                        <div class="mt-2">
                            <input class="input" name="nextID" type="text" disabled placeholder="Id" value="<?php echo !empty($nextID) ? $nextID : ''; ?>">
                        </div>
                    </div>
                    <div class="column is-one-fifth mx-4">
                        <div class="<?php echo !empty($codigoPostalError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">Código Postal*</label>
                        </div>
                        <div class="mt-2">
                            <input class="input" type="number" placeholder="ej. 72542" id="cp" name="codigoPostal" value="<?php echo !empty($codigoPostal) ? $codigoPostal : ''; ?>">
                            <?php if (($codigoPostalError != null)) ?>
                            <span class="help-inline"><?php echo $codigoPostalError; ?></span>
                        </div>
                    </div>
                    <div class="column is-one-fifth mx-4">
                        <div class="<?php echo !empty($seccionElectoralError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">Sección Electoral*</label>
                        </div>
                        <div class="mt-2">
                            <input class="input" type="number" placeholder="e.g. 2571" id="iptSeccion" name="seccionElectoral" value="<?php echo !empty($seccionElectoral) ? $seccionElectoral : ''; ?>">
                            <?php if (($seccionElectoralError != null)) ?>
                            <span class="help-inline"><?php echo $seccionElectoralError; ?></span>
                        </div>
                    </div>
                </div>

                <div class="columns is-centered mt-5 mx-6">
                    <div class="column is-one-fifth mx-3">
                        <div class="mt-2 ml-6 <?php echo !empty($idSexoError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">Sexo*</label>
                        </div>
                        <div class="select is-6 pl-6" name="Sexo">
                            <select name="idSexo">
                                <option>Seleccionar</option>
                                <?php
                                $pdo = Database::connect();
                                $query = 'SELECT * FROM sexoOpciones';
                                foreach ($pdo->query($query) as $row) {
                                    if ($row['idSexo'] == $idSexo)
                                        echo "<option selected value='" . $row['idSexo'] . "'>" . $row['tipoSexo'] . "</option>";
                                    else
                                        echo "<option value='" . $row['idSexo'] . "'>" . $row['tipoSexo'] . "</option>";
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <?php if (($idSexoError) != null) ?>
                            <span class="help-inline"><?php echo $idSexoError; ?></span>
                        </div>
                    </div>
                    <div class="column is-one-third mx-3">
                        <div class="mt-2 ml-6 <?php echo !empty($idOcupacionError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">Ocupación*</label>
                        </div>
                        <div class="select is-6 pl-6" name="Ocupación">
                            <select name="idOcupacion">
                                <option>Seleccionar</option>
                                <?php
                                $pdo = Database::connect();
                                $query = 'SELECT * FROM ocupacionOpciones';
                                foreach ($pdo->query($query) as $row) {
                                    if ($row['idOcupacion'] == $idOcupacion)
                                        echo "<option selected value='" . $row['idOcupacion'] . "'>" . $row['tipoOcupacion'] . "</option>";
                                    else
                                        echo "<option value='" . $row['idOcupacion'] . "'>" . $row['tipoOcupacion'] . "</option>";
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <?php if (($idOcupacionError) != null) ?>
                            <span class="help-inline"><?php echo $idOcupacionError; ?></span>
                        </div>
                    </div>
                    <div class="column is-one-fifth mx-3">
                        <div class="mt-2 ml-6 <?php echo !empty($idEscolaridadError) ? 'error' : ''; ?>"">
                        <label class=" label is-size-5">Escolaridad*</label>
                        </div>
                        <div class="select is-6 pl-6" name="Escolaridad">
                            <select name="idEscolaridad">
                                <option>Seleccionar</option>
                                <?php
                                $pdo = Database::connect();
                                $query = 'SELECT * FROM escolaridadOpciones';
                                foreach ($pdo->query($query) as $row) {
                                    if ($row['idEscolaridad'] == $idEscolaridad)
                                        echo "<option selected value='" . $row['idEscolaridad'] . "'>" . $row['tipoEscolaridad'] . "</option>";
                                    else
                                        echo "<option value='" . $row['idEscolaridad'] . "'>" . $row['tipoEscolaridad'] . "</option>";
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <?php if (($idEscolaridadError) != null) ?>
                            <span class="help-inline"><?php echo $idEscolaridadError; ?></span>
                        </div>
                    </div>
                </div>
                </br>
                </br>
        </div>

        <!-- Inicio Pregunta 1 -->
        <div>
            <div class="divider">Pregunta 1</div>
        </div>

        <div class="container pregunta1 <?php echo !empty($narrativaError) ? 'error' : ''; ?>" style="width: 1000px; height: 200px;">
            <p class="subtitle is-4 has-text-weight-semibold pt-3 mx-6">
                Explíquenos ¿Qué sucedió?*
            </p>
            <textarea required class="textarea is-focused" type="text" name="narrativa" placeholder="Favor de relatar aquí una breve narrativa de los hechos." value="<?php echo !empty($narrativa) ? $narrativa : ''; ?>"></textarea>
            <?php if (($narrativaError != null)) ?>
            <span class="help-inline"><?php echo $narrativaError; ?></span>
        </div>
        </br>
        </br>
        <!-- Fin Pregunta 1 -->

        <!-- Inicio Pregunta 2 -->
        <div>
            <div class="divider">Pregunta 2</div>
        </div>
        <div class="container pregunta2" style="width: 1000px; height: 200px;">
            <p class="subtitle is-4 has-text-weight-semibold pt-3 mx-6">
                ¿Dónde ocurrió este hecho?*
            </p>
            </br>

            <div class="field is-horizontal mt-1 ml-6">
                <div class="field-body mx-6">
                    <div class="control mr-6 mt-1 ml-6">
                        <label class="label is-size-5">Estado*</label>
                    </div>
                    <div class="field mr-3">
                        <div class="select" name="estado">
                            <select required name="estadoSucedio" id="jmr_contacto_estado">
                            </select>
                        </div>
                    </div>

                    <div class="control mr-6 mt-1 ml-6">
                        <label class="label is-size-5">Municipio*</label>
                    </div>
                    <div class="field mr-3">
                        <div class="select" name="municipio">
                            <select required name="municipioSucedio" id="jmr_contacto_municipio">
                                <option>------------------</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Fin Pregunta 2 -->
        <!-- Inicio  Pregunta 3 -->
        <div>
            <div class="divider">Pregunta 3</div>
        </div>
        <div class="container pregunta3" style="width: 1000px; height: 200px;">
            <p class="subtitle is-4 has-text-weight-semibold pt-3 mx-6">
                ¿Cuándo sucedió?*
            </p>
            </br>

            <div class="field is-horizontal mt-1 ml-6">
                <div class="field-body mx-6">
                    <div class="control mr-6 mt-1 ml-6 ">
                        <label class="label is-size-5">Fecha*</label>
                    </div>
                    <div class="field">
                        <!-- <input name="fechaSucedio" class="input" type="date" data-color="info"> -->
                        <input name="fechaSucedio" class="input" type="text" placeholder="DD/MM/AAAA">
                    </div>
                </div>

                <div class="control mr-6 mt-1 ml-6">
                    <label class="label is-size-5">Hora aprox.*</label>
                </div>
                <div class="field">
                    <!-- <input name="horaAproxSucedio" class="input" type="time" data-color="info" data-validate-label="Aceptar" data-clear-label="Limpiar" data-cancel-label="Cancelar"> -->
                    <input name="horaAproxSucedio" class="input" type="text" placeholder="HH:MM">
                </div>
            </div>
        </div>
        <!-- Fin  Pregunta 3 -->

        <!-- Inicio  Pregunta 4 -->
        <div>
            <div class="divider">Pregunta 4</div>
        </div>
        <div class="container pregunta4" style="width: 1000px; height: 400px;">
            <p class="subtitle is-4 has-text-weight-semibold pt-3 mx-6">
                Si es posible, ¿Podría proporcionarnos los datos de la persona que lo cometió?
            </p>
            </br>

            <div class="field is-horizontal mt-1 ml-6">
                <div class="column pregunta4">
                    <div class="columns mt-1">
                        <div class="column is-5 has-text-right mt-1 <?php echo !empty($nombreQuienComError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">Nombre </label>
                        </div>
                        <div class="column is-5">
                            <input class="input" type="text" name="nombreQuienCom" value="<?php echo !empty($nombreQuienCom) ? $nombreQuienCom : ''; ?>">
                            <?php if (($nombreQuienComError != null)) ?>
                            <span class="help-inline"><?php echo $nombreQuienComError; ?></span>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column is-5 has-text-right mt-1 <?php echo !empty($empresaQuienComError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">Institución en la que labora</label>
                        </div>
                        <div class="column is-5">
                            <input class="input" type="text" name="empresaQuienCom" value="<?php echo !empty($empresaQuienCom) ? $empresaQuienCom : ''; ?>">
                            <?php if (($empresaQuienComError != null)) ?>
                            <span class="help-inline"><?php echo $empresaQuienComError; ?></span>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column is-5 has-text-right mt-1 <?php echo !empty($puestoQuienComError) ? 'error' : ''; ?>">
                            <label class="label is-size-5">¿Sabe cuál es su rol en la institución?</label>
                        </div>
                        <div class="column is-5 mb-4">
                            <input class="input" type="text" name="puestoQuienCom" value="<?php echo !empty($puestoQuienCom) ? $puestoQuienCom : ''; ?>">
                            <?php if (($puestoQuienComError != null)) ?>
                            <span class="help-inline"><?php echo $puestoQuienComError; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin  Pregunta 4 -->

        <!-- Inicio  Pregunta 5 -->
        <div>
            <div class="divider">Pregunta 5</div>
        </div>
        <div class="container pregunta5" style="width: 1000px; height: 400px;">
            <p class="subtitle is-4 has-text-weight-semibold pt-3 mx-6">
                ¿Qué conductas considera que se cometieron?
            </p>
            </br>

            <div class="field is-horizontal mt-1 ml-6">
                <div class="column pregunta5">
                    <div class="columns">
                        <!-- Checkboxes -->
                        <div class="column is-5 ml-4 ">
                            <label class="checkbox has-tooltip-multiline  is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit viverra mattis conubia, aptent a potenti porttitor sociosqu parturient blandit feugiat aliquam in, pulvinar purus gravida eleifend hac auctor faucibus rhoncus donec.">
                                <input name="compraVotos" type="checkbox" value="True" <?php $compraVotos = null;
                                echo ($compraVotos == "True") ? 'checked' : ''; ?>>
                                Compra de votos
                            </label>
                        </div>
                        <div class="column is-6 has-text-centered ml-1 pr-6">
                            <label class="checkbox has-tooltip-multiline  is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit in tortor arcu, a dapibus cum elementum velit mi ante libero sapien lacus, augue fusce pharetra natoque nisi litora justo montes leo.">
                                <input name="tomaCredencial" type="checkbox" value="True" <?php $tomaCredencial = null;
                                echo ($tomaCredencial == "True") ? 'checked' : ''; ?>>
                                Condicionar un servicio público
                            </label>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column is-5 ml-4 pr-1">
                            <label class="checkbox has-tooltip-multiline  is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit bibendum aliquet dui, donec hac platea luctus pharetra malesuada diam fermentum. Fringilla justo est himenaeos eros morbi semper, porttitor senectus augue at.">
                                <input name="condicionarServ" type="checkbox" value="True" <?php $condicionarServ = null;
                                echo ($condicionarServ == "True") ? 'checked' : ''; ?>>
                                Tomar INE sin consentimiento
                            </label>
                        </div>
                        <div class="column is-6 has-text-centered">
                            <label class="checkbox has-tooltip-multiline  is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit ridiculus risus tellus, tempus sem orci duis mauris urna rutrum cras lobortis volutpat, commodo facilisi imperdiet a aptent etiam euismod nam pellentesque.">
                                <input name="alterarResultados" type="checkbox" value="True" <?php $alterarResultados = null;
                                echo ($alterarResultados == "True") ? 'checked' : ''; ?>>
                                Alteración de resultados electorales
                            </label>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column is-5 ml-4 mr-4">
                            <label class="checkbox has-tooltip-multiline is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit scelerisque accumsan imperdiet, vehicula arcu suspendisse suscipit fusce eros dui pharetra tristique curae, praesent mattis integer vitae faucibus mollis varius viverra dictum.">
                                <input name="roboCasillas" type="checkbox" value="True" <?php $roboCasillas = null;
                                echo ($roboCasillas == "True") ? 'checked' : ''; ?>>
                                Robo de casillas electorales
                            </label>
                        </div>
                        <div class="column is-6 has-text-centered">
                            <label class="checkbox has-tooltip-multiline  is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit, potenti metus class at pulvinar in tristique, posuere massa cursus duis sodales non. Arcu curae nunc phasellus urna rutrum, nullam mauris quisque.">
                                <input name="amenazaServPub" type="checkbox" value="True" <?php $amenazaServPub = null;
                                echo ($amenazaServPub == "True") ? 'checked' : ''; ?>>
                                Amenazas por parte de servidor público
                            </label>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column is-1 ml-4 mt-1">
                            <label class="has-tooltip-multiline  is-size-5" data-tooltip="Lorem ipsum dolor sit amet consectetur adipiscing elit imperdiet fames, per primis vivamus et nullam nulla curabitur augue. Sollicitudin sem tempor risus conubia duis senectus, orci purus dignissim mollis nam.">Otra</label>
                        </div>
                        <div class="column is-5">
                            <input name="otroConductaCom" class="input" type="text" value="<?php echo !empty($otroConductaCom) ? $otroConductaCom : ''; ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin  Pregunta 5 -->

        <!-- Inicio  Pregunta 6 -->
        <div>
            <div class="divider">Pregunta 6</div>
        </div>
        <div class="container pregunta6" style="width: 1000px; height: 400px;">
            <p class="subtitle is-4 has-text-weight-semibold pt-3 mx-6">
                Evidencias
            </p>
            <p class="subtitle is-6 has-text-centered is-size-5">
                Si tiene alguna evidencia (foto, video, audio, documento), por favor adjúntelo a continuación.
            </p>
            </br>
            </br>
            <div class="columns mt-1 is-centered">
                <!-- file upload -->
                <div id="file-js-example" class="file has-name">
                    <label class="file-label">
                        <input class="file-input" type="file" name="resume">
                        <span class="file-cta">
                            <span class="file-icon">
                                <i class="fas fa-upload"></i>
                            </span>
                            <span class="file-label">
                                Escoge un archivo…
                            </span>
                        </span>
                        <span class="file-name">
                            Ningún archivo seleccionado
                        </span>
                    </label>
                </div>
            </div>
            </br>
            </br>
            </br>
            </br>

            <!-- Inicio de boton de enviar -->
            <div class="form-actions content has-text-centered">
                <button id="btnEnviar" class="button buttonB is-large" type="submit" name="submit">Enviar</button>
                </br>
            </div>
            <!-- Fin de boton de enviar -->
            </br>
            </br>
            </br>
            </br>
        </div>
        <!-- Fin  Pregunta 6 -->

        </form>
    </section>
    </br>
    </br>
    </br>
    </br>
    </br>

    <section class="hero-foot pt-6">
        <!-- div que contiene el footer -->
        <div id="footer"></div>
    </section>

</body>

<!-- Script que carga los archivos y modal de politica de privacidad -->
<script>
    $("#nav-bar").load("shared/navbar2.php");
    $("#footer").load("shared/footer.php");

    //Script para cargar municipios y estados
    $(document).ready(function() {
        // Obtener estados
        $.ajax({
            type: "POST",
            url: "procesar-estados.php",
            data: {
                estados: "Mexico"
            }
        }).done(function(data) {
            $("#jmr_contacto_estado").html(data);
        });
        // Obtener municipios
        $("#jmr_contacto_estado").change(function() {
            var estado = $("#jmr_contacto_estado option:selected").val();
            //alert(estado);
            $.ajax({
                type: "POST",
                url: "procesar-estados.php",
                data: {
                    municipios: estado
                }
            }).done(function(data) {
                $("#jmr_contacto_municipio").html(data);
            });
        });
        $("#jmr_contacto_municipio").change(function() {
            var municipio = $("#jmr_contacto_municipio option:selected").val();
            //alert(municipio);
        });
    });

    // Ventana modal de politica de privacidad al clic en boton siguiente
    $("#btnSiguiente").hover(function() {
        const cam1 = document.getElementById('cp');
        const cam2 = document.getElementById('iptSeccion');
        if (cam1.value.length != 0 && cam2.value.length != 0) {
            Swal.fire({
                text: 'La información proporcionada en este formulario está asegurada bajo la política de privacidad de Solo para Incorruptibles, y solo se utilizará para fines de este servicio',
                icon: 'warning',
                showCloseButton: true,
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "formularioPreg1.php";
                }
            })
        }
    });

    // Ventana modal de politica de privacidad al clic en boton siguiente
    $("#iconHelp").on("click", function() {
        Swal.fire({
            imageUrl: 'img/ine_muestra.png',
            text: 'La sección electoral se encuentra en la parte frontal de la credencial de elector, así como se muestra en la imagen',
            showCloseButton: true,
            confirmButtonText: 'Ok',
            confirmButtonColor: '#2B2D42'
        })
    });

    var calendars = bulmaCalendar.attach('[type="date"]');
    var calendars = bulmaCalendar.attach('[type="time"]');

    // Subir archivos
    const fileInput = document.querySelector('#file-js-example input[type=file]');
    fileInput.onchange = () => {
        if (fileInput.files.length > 0) {
            const fileName = document.querySelector('#file-js-example .file-name');
            fileName.textContent = fileInput.files[0].name;
        }
    }

    // Modal de tiempo que se muestra cuando el formulario se ha enviado exitosamente
    // TODO: bandera para enviar modales diferentes, si ocurre error en bd, mandar mensaje de error
    /*$("#btnEnviar").on("click", function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1500,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'success',
            title: 'Formulado enviado exitosamente'
        }).then((result) => {
            window.location.href = "denuncias.php";
        })
    });*/
</script>

</html>