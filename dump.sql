CREATE DATABASE denuncias;

CREATE TABLE sexoOpciones(
	idSexo INT PRIMARY KEY,
	tipoSexo VARCHAR(50)
);

INSERT INTO sexoOpciones VALUES
(1, "Hombre"),
(2, "Mujer"),
(3, "Prefiero no decirlo");

CREATE TABLE ocupacionOpciones(
	idOcupacion INT PRIMARY KEY,
	tipoOcupacion VARCHAR(100)
);

INSERT INTO ocupacionOpciones VALUES
(1, "Funcionarios público"),
(2, "Profesionista"),
(3, "Trabajador administrativo"),
(4, "Comerciantes o agente de ventas"),
(5, "Trabajadores en servicios personales y de vigilancia"),
(6, "Trabajadores en actividades agrícolas, ganaderas, forestales, caza y pesca"),
(7, "Trabajadores artesanales, en la construcción y otros oficios"),
(8, "Operadores de maquinaria industrial, ensambladores, choferes y conductores de transporte"),
(9, "Trabajadores en actividades elementales y de apoyo");


CREATE TABLE escolaridadOpciones(
	idEscolaridad INT PRIMARY KEY,
	tipoEscolaridad VARCHAR(50)
);

INSERT INTO escolaridadOpciones VALUES
(1, "Primaria"),
(2, "Secundaria"),
(3, "Preparatoria"),
(4, "Universidad"),
(5, "Posgrado"),
(6, "Otro");

CREATE TABLE 1datosDenunciante(
	idDatosDenunciante INT PRIMARY KEY AUTO_INCREMENT,
	codigoPostal INT,
	seccionElectoral INT,
	ocupacion INT,
	escolaridad INT,
	sexo INT,
	FOREIGN KEY (ocupacion) REFERENCES ocupacionOpciones(idOcupacion),
	FOREIGN KEY (escolaridad) REFERENCES escolaridadOpciones(idEscolaridad),
	FOREIGN KEY (sexo) REFERENCES sexoOpciones(idSexo)
);

CREATE TABLE 2queSucedio(
	idQueSucedio INT PRIMARY KEY AUTO_INCREMENT,
	narrativa VARCHAR(1000)
);

CREATE TABLE 3dondeSucedio(
	idDondeSucedio INT PRIMARY KEY AUTO_INCREMENT,
	estadoSucedio VARCHAR(100),
	municipioSucedio VARCHAR(100)
);

CREATE TABLE 4cuandoSucedio(
	idCuandoSucedio INT PRIMARY KEY AUTO_INCREMENT,
	fechaSucedio VARCHAR(15),
	horaAproxSucedio VARCHAR(10)
);

CREATE TABLE 5quienCometio(
	idQuienCometio INT PRIMARY KEY AUTO_INCREMENT,
	nombreQuienCom VARCHAR(100),
	empresaQuienCom VARCHAR(100),
	puestoQuienCom VARCHAR(100)
);

CREATE TABLE 6queConductaCom(
	idConductaCometio INT PRIMARY KEY AUTO_INCREMENT,
	compraVotos VARCHAR(10),
	tomaCredencial VARCHAR(10),
	condicionarServ VARCHAR(10),
	alterarResultados VARCHAR(10),
	roboCasillas VARCHAR(10),
	amenazaServPub VARCHAR(10),
	otroConductaCom VARCHAR(500)
);

CREATE TABLE 7evidencias(
	idEvidencia INT PRIMARY KEY AUTO_INCREMENT,
	subirEvidencia VARCHAR(1000)
);

CREATE TABLE estatusDenuncia(
	idestatusDenuncia INT PRIMARY KEY,
	nombreEstatus VARCHAR(50)
);

INSERT INTO estatusDenuncia VALUES
(1, "Denuncia recibida por elección sin corrupción "),
(2, "Denuncia enviada "),
(3, "Denuncia en institución correspondiente");

CREATE TABLE denunciasConEstatus(
	idDenunciaConEstatus INT PRIMARY KEY AUTO_INCREMENT,
	idEstatusDeDenuncia INT,
	FOREIGN KEY (idEstatusDeDenuncia) REFERENCES estatusDenuncia(idestatusDenuncia)
);
