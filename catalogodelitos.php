<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>

</head>

<body>
    <!-- div que contiene la barra de navegación -->
    <div id="nav-bar"></div>
    </br>
    </br>
    <!-- Inicio de div que contiene cards en columnas para la implementación de lecturas -->

    <h1 class="title is-2 has-text-centered has-text-weight-semibold pt-6">Catálogo de delitos electorales</h1>
    </br>
    </br>
    <!-- Inicio de primera fila de 4 columnas para la presentación de lecturas -->
    <div class="columns px-6">

        <!-- Inicio de columna para card # 1 -->
        <div class="column is-one-third">
            <div class="card" style="height:100%">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Investigaciones</p>
                            <p class="subtitle is-6">Autor de lectura</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 1 -->

        <!-- Inicio de columna para card # 2 -->
        <div class="column column is-one-third">
            <div class="card" style="height:100%">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Investigaciones</p>
                            <p class="subtitle is-6">Autor de lectura</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 2 -->

        <!-- Inicio de columna para card # 3 -->
        <div class="column column is-one-third">
            <div class="card" style="height:100%">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Investigaciones</p>
                            <p class="subtitle is-6">Autor de lectura</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 3 -->

    </div>
    <!--  Fin de primera fila de 4 columnas para la presentación de lecturas -->

    <!-- Inicio de segunda fila de 4 columnas para la presentación de lecturas -->
    <div class="columns px-6">

        <!-- Inicio de columna para card # 4 -->
        <div class="column">
            <div class="card" style="height:100%">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Investigaciones</p>
                            <p class="subtitle is-6">Autor de lectura</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 4 -->

        <!-- Inicio de columna para card # 5 -->
        <div class="column">
            <div class="card" style="height:100%">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Investigaciones</p>
                            <p class="subtitle is-6">Autor de lectura</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 5-->

        <!-- Inicio de columna para card #6 -->
        <div class="column">
            <div class="card" style="height:100%">
                <div class="card-image">
                    <a id="imagen4">
                        <figure class="image is-4by3">
                            <img src="img/imagen3.jpg">
                        </figure>
                    </a>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Investigaciones</p>
                            <p class="subtitle is-6">Autor de lectura</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 6 -->

    </div>
    <!--  Fin de segunda fila de 4 columnas para la presentación de lecturas -->
    </br>
    </br>
    </br>
    </br>

    <!-- Fin de div que contiene cards en columnas para la implementación de lecturas -->
    <div id="footer"></div>
</body>

<!-- Script que carga los archivos -->
<script>
    $("#footer").load("shared/footer.php");
    $("#nav-bar").load("shared/navbar.php");

    $("#imagen4").on("click", function() {
        Swal.fire({

            imageUrl: 'img/ine_muestra.png',
            imageWidht: 600,
            confirmButtonText: 'Ok',
            confirmButtonColor: '#2B2D42'
        })
    });
</script>

</html>