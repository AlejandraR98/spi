<!DOCTYPE html>
<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'shared/meta_links.php'; ?>
</head>

<body>
    <!-- div que contiene la barra de navegación -->
    <div id="nav-bar"></div>
    </br>
    </br>
    <!-- Inicio de div que contiene cards en columnas para la implementación de lecturas -->

    <h1 class="title is-2 has-text-centered has-text-weight-semibold pt-6">InformaTEC</h1>
    </br>
    </br>
    <!-- Inicio de primera fila de 4 columnas para la presentación de lecturas -->
    <div class="columns px-6">

        <!-- Inicio de columna para card # 1 -->
        <div class="column is-one-third">
            <div class="card">
                <div class="card-image">
                    <iframe width="100%" height="320" src="https://www.youtube.com/embed/7qdIPz47Fpw" frameborder="0" allow="accelerometer;
                            autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Informatec Maestra </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 1 -->

        <!-- Inicio de columna para card # 2 -->
        <div class="column column is-one-third">
            <div class="card">
                <div class="card-image">
                    <iframe width="100%" height="320" src="https://www.youtube.com/embed/7qdIPz47Fpw" frameborder="0" allow="accelerometer;
                          autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Informatec Maestra </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 2 -->

        <!-- Inicio de columna para card # 3 -->
        <div class="column column is-one-third">
            <div class="card">
                <div class="card-image">
                    <iframe width="100%" height="320" src="https://www.youtube.com/embed/7qdIPz47Fpw" frameborder="0" allow="accelerometer;
                          autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Informatec Maestra </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 3 -->

    </div>
    <!--  Fin de primera fila de 4 columnas para la presentación de lecturas -->

    <!-- Inicio de segunda fila de 4 columnas para la presentación de lecturas -->
    <div class="columns px-6">

        <!-- Inicio de columna para card # 4 -->
        <div class="column">
            <div class="card">
                <div class="card-image">
                    <iframe width="100%" height="320" src="https://www.youtube.com/embed/7qdIPz47Fpw" frameborder="0" allow="accelerometer;
                          autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Informatec Maestra </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 4 -->

        <!-- Inicio de columna para card # 5 -->
        <div class="column">
            <div class="card">
                <div class="card-image">
                    <iframe width="100%" height="320" src="https://www.youtube.com/embed/7qdIPz47Fpw" frameborder="0" allow="accelerometer;
                          autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Informatec Maestra </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 5-->

        <!-- Inicio de columna para card #6 -->
        <div class="column">
            <div class="card">
                <div class="card-image">
                    <iframe width="100%" height="320" src="https://www.youtube.com/embed/7qdIPz47Fpw" frameborder="0" allow="accelerometer;
                          autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-content">
                            <p class="title is-4">Informatec Maestra </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin de columna para card # 6 -->

    </div>
    <!--  Fin de segunda fila de 4 columnas para la presentación de lecturas -->
    </br>
    </br>
    </br>
    </br>

    <!-- Fin de div que contiene cards en columnas para la implementación de lecturas -->
    <div id="footer"></div>
</body>

<!-- Script que carga los archivos -->
<script>
    $("#footer").load("shared/footer.php");
    $("#nav-bar").load("shared/navbar.php");
</script>

</html>