<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'meta_links.php'; ?>
</head>

<body>
    <!-- Inicio de carrusel pl-3-->
    <div id="carousel" class="hero-carousel is-medium" style="width: 100% ">
        <div class="item-1 car-item  ">
          <figure class="image has-image-centered">
      <img src="img/imagen1.jpg">
    </figure>

        </div>
        <div class="item-2 car-item ">

          <figure class="image ">
            <img src="img/imagen2.jpeg " >
</figure>
        </div>
        <div class="item-3 car-item is-horizontal-center ">
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            <div class="content has-text-centered">
                <h2>
                    ¿Alguien está atentando contra tu voto?
                </h2>
                <a href="denuncias.php">
                    <button id="btnDenuncia" class="button is-large is-rounded" type="submit" name="submit" style="width: 500px; height: 100px;">
                        Denuncia aquí
                    </button>
                </a>
            </div>
        </div>
        <div class="item-4 car-item is-horizontal-center ">
            <img src="img/imagen4.jpg">
        </div>
        <div class="item-5 car-item is-horizontal-center ">
            <img src="img/imagen5.jpg">
        </div>
    </div>
    <div class="hero-body"></div>
    <!-- Fin de implementación de carrusel -->

    <!-- Inicio de configuración de carrusel -->
    <script>
        bulmaCarousel.attach('#carousel', {
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            pauseOnHover: false,
            slidesToScroll: 1,
            slidesToShow: 1,
            breakpoints: [{
                changePoint: 480,
                slidesToShow: 1,
                slidesToScroll: 1
            }, {
                changePoint: 640,
                slidesToShow: 1,
                slidesToScroll: 2
            }, {
                changePoint: 768,
                slidesToShow: 1,
                slidesToScroll: 3
            }]
        });
    </script>
    <!-- Fin de configuración de carrusel -->

</body>

</html>
