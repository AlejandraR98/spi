<html>

<head>
    <!-- Archivo que incluye los meta links -->
    <?php include 'meta_links.php'; ?>
</head>

<body>
    <!-- Inicio de la barra de navegacion anclada en la parte superior de la pagina -->
    <nav class="navbar is-fixed-top is-transparent" role="navigation" aria-label="main navigation">

        <!-- Inicio de logo que lleva al index -->
        <a href="index.php" class="navbar-menu">
            <img class="img-logo mx-6" src="img/logo.png" width="70" height="62">
        </a>
        <!-- Fin del logo -->

        <!-- Inicio de tabs de barra de navegación implementadas al final de esta -->
        <div class="menu navbar-end">
            <a class="navbar-item pl-5 pr-6" href="index.php">
                Inicio
            </a>

            <a class="navbar-item pl-5 pr-6" href="index.php#descripcion-grupo">
                Acerca de
            </a>

            <a class="navbar-item pl-5 pr-6" href="informacion.php">
                Elección sin corrupción
            </a>
            <a class="navbar-item pl-5 pr-6" href="index.php#lecturas">
                Infórmate
            </a>
            <a class="navbar-item pl-5 pr-6" href="interactivo.php">
                Área Interactiva
            </a>

            <!-- Inico de iconos de redes sociales en barra de navegación -->
            <div id="icons" class="buttons is-centered social-icons pl-5 pr-6">
                <a class="icon is-large is-facebook" href="https://www.facebook.com/spincorruptibles">
                    <i class="fab fa-lg fa-facebook-f"></i>
                </a>
                <a class="icon is-large" href="https://twitter.com/spincorruptible">
                    <i class="fab fa-twitter fa-lg"></i>
                </a>
                <a class="icon is-large" href="https://www.instagram.com/spincorruptibles">
                    <i class="fab fa-instagram fa-lg"></i>
                </a>
            </div>
            <!-- Fin de iconos de redes sociales en barra de navegación -->
        </div>
        <!-- Fin de tabs de barra de navegación -->
    </nav>
    <!-- Fin de la barra de navegacion -->

</body>

</html>